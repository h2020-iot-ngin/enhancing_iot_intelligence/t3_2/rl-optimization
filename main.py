import logging
from src.smart_grid_env import SmartGridEnvironment
from src.agent import TensorforceAgent
from utils.callbacks import Callbacks

logging.basicConfig(level=logging.INFO)

AGENT_JSON = "src/agents/agent.json"
AGENT_JSON = "src/agents/agent_ppo.json"
TENSORBOARD_DIR = 'logs/smart-energy/'
EPISODES = 2
TENSORBOARD_FILE = "test-2"

#Instantiate Tensorboard callback
tb_callback = Callbacks(TENSORBOARD_DIR)
# Instantiate an Environment
environment = SmartGridEnvironment(max_steps=5)

network=dict(
            type="auto",
            size=64,
            depth=4,
        )
# network=dict(network=[
#         dict(type='dense', size=128, activation='tanh'),
#         dict(type='dense', size=64, activation='tanh'),
#         dict(type='dense', size=64, activation='tanh')
#  ])
# Instantiate a Tensorforce agent
agent = TensorforceAgent(
                AGENT_JSON,
                environment,
                network=network
            ).get_agent()


best_states = None
for episode in range(EPISODES):
    # Initialize episode
    BEST_REWARD = 0
    states = environment.reset()
    terminal = False
    tb_callback.on_episode_start(episode, states)
    while not terminal:
        # Episode timestep
        actions = agent.act(states=states)
        states, terminal, reward = environment.execute(actions=actions)
        if reward > BEST_REWARD:
            BEST_REWARD = reward
            best_states = states
        agent.observe(terminal=terminal, reward=reward)
        logging.info(f"Action: {actions}.\nStates:\n{states}.\n\nReward:\n{reward}")
        tb_callback.on_step_end(reward)
    tb_callback.on_best_reward(best_states)
    tb_callback.on_episode_end(reward,states)
    logging.info(f"Episode: {episode}: reward: {reward}")

agent.close()
environment.close()