# RL for Smart Grid Optimization

This project aims to optimize an electric grid through RL agent. The agent performs energy shifts on different loads along the day to maximize the electric grid performance. The performance is measured by SSR (Self Sufficiency Rate) and SCR (Self Consumpsion Rate).

## Getting Started
### Prerequisites

The following dependencies are required to run the training:
- Tensorforce
- Numpy 
- Pandas
- PandaPower

Version of each python package can be found here: [requeriments.txt](requirements.txt). 

Run this command to install the dependencies:
```
pip install -r requirements.txt
```

### Usage and Configuration
The following parameters can be configured in the script:

- AGENT_JSON: The path to the agent configuration *JSON* file. (Agent tested can be found here: [agents](src/agents/))
- TENSORBOARD_DIR: The directory to store Tensorboard logs.
- TENSORBOARD_FILE: The name of the Tensorboard file.
- EPISODES: The number of episodes to run the training for.
Additionally, you can configure the network architecture of the agent by modifying the network variable in the script. By default, the agent uses a four-layer fully connected neural network with 64 units per layer.

To run the training, simply run the following command:

```
ptyhon main.py
```

## Implementation

### Environment
The environment is implemented at [smart_grid_env.py](src/smart_grid_env.py). The module defines a class called *SmartGridEnvironment*, which is a subclass of tensorforce.*environments.Environment*. The *SmartGridEnvironment* class is used to provide a wrapper for a reinforcement learning agent based on the Tensorforce framework.

It is responsible of executing the actions took by the agent on the environment and provide new states and the reward.

### Agent
The agent definition is implemented at [agent.py](src/agent.py). This module defines a Python class called *TensorforceAgent* which is a wrapper for the Tensorforce reinforcement learning agent. The class provides a convenient way to create and load a Tensorforce agent object from a *JSON* configuration file and to access the agent object to use in training and testing reinforcement learning models.

It takes three arguments:
- *agent_path*: Path to the *JSON* file defining the agent's configuration.
- *environment*: Environment argument is an instance of the environment to use for training the agent
- *network*: Configuration dictionary of the neural network to use as the agent's policy. The network argument is optional and defaults to 'auto'.  

### Simulator
The smart grid simulator is implemented here: [rl_smart_grid_simulator.py](src/simulator/rl_smart_grid_simulator.py). This module creates a *Pandapower* electric grid object with the characteristics defined at [netowrk_function.py](src/simulator/network_function.py). It simulates actions took by the agent, compute new states and the reward.

### Training
Training loop involving previous modules is implemented at [main.py](main.py).