import pandapower as pp
import numpy as np
import pandas as pd
import logging
import src.simulator.network_function as network_function

logging.basicConfig(level=logging.INFO)

class SmartGridSimulator():

    def __init__(self,base_path="src/simulator/data", max_steps=40):
        # Smart grid initialization
        self.net = network_function.iotngin_network()
        # KPIs initialization
        self.energy_losses=None
        self.energy_produced=None
        self.energy_exported=None
        self.energy_consumed=None
        self.energy_exchanged=None
        self.LOADS_D = None
        self.LOADS_I = None
        self.PV = None
        self.SSR = None
        self.SCR = None
        
        self.n_steps=0
        self.max_steps=max_steps
        self.load = "Load_D_486"
        self.read_initial_loads(base_path)

    def read_initial_loads(self, base_path):
        # Reading consumption and production files.
        self.LOADS_D = pd.read_csv(base_path+'/RESIDENTIAL.csv')
        self.LOADS_I = pd.read_csv(base_path+'/INDUSTRIAL.csv')
        self.PV = pd.read_csv(base_path+'/PV_GENERATION.csv')
    
    def compute_domestic_mean_distribution(self):
        return self.LOADS_D.sum(axis=1) / sum(self.LOADS_D.sum(axis=1))
        # self.LOADS_I.sum(axis=1) / sum(self.LOADS_I.sum(axis=1))
    
    def compute_industrial_mean_distribution(self):
        return self.LOADS_I.sum(axis=1) / sum(self.LOADS_I.sum(axis=1))

    def get_states(self):
        # return [self.energy_produced, self.energy_consumed]
        return [self.energy_exchanged]

    def get_action_set(self):
        actions = {
            # "load":np.arange(0,26,1).tolist(),
            "energy_shift":[0.25, 0.5],
            "initial_window":np.arange(0,96,4).tolist(),
            "final_window":[1,2]
            # "final_window":np.arange(0,96,4).tolist()
        }
        return actions
    
    def finish_simulation(self):
        if self.n_steps >= self.max_steps-1:
            return True
        return False

    def __simulation_logs__(self):
        logging.info(f"energy consumed = {sum(self.energy_consumed)} MWh")
        logging.info(f"energy produced = {sum(self.energy_produced)} MWh")
        logging.info(f"energy exported = {sum(self.energy_exported)} MWh")
        logging.info(f"energy exchanged = {sum(self.energy_exchanged)} MWh")
        logging.info(f"energy losses = {sum(self.energy_losses)} MWh")
        logging.info(f"energy losses in % = {100*(sum(self.energy_losses))/(sum(self.energy_consumed))} MWh")
        logging.info(f" the self consumption rate is {self.SCR} %")
        logging.info(f" the self sufficiency rate is {self.SSR} %")

    def get_energy_shift(self, shift):
        actions = self.get_action_set()
        return actions["energy_shift"][shift]

    def get_selected_load(self, load=0):
        sum_df = pd.DataFrame()
        sum_df["sum"] = self.LOADS_D.sum().sort_values(ascending=False)
        idx_D = sum_df.iloc[load].name
        sum_df = pd.DataFrame()
        sum_df["sum"] = self.LOADS_I.sum().sort_values(ascending=False)
        idx_I = sum_df.iloc[load].name
        return idx_D, idx_I
        # return self.LOADS_D.columns[idx_D], self.LOADS_I.columns[idx_I]
        # return self.load

    def get_initial_window(self, init_window):
        actions = self.get_action_set()
        return actions["initial_window"][init_window]

    def get_final_window(self, final_window):
        actions = self.get_action_set()
        return actions["final_window"][final_window]

    def compute_shift_power(self,load,initial_window,energy_shift):
        if "Load_D" in load:
            return self.LOADS_D[load].loc[initial_window:initial_window+3].sum()*energy_shift
        else:
            return self.LOADS_I[load].loc[initial_window:initial_window+3].sum()*energy_shift

    def energy_shift_performance(self, actions):
        # 1- load selection
        # 2- Initial window extraction
        # 3- Final window extraction
        # 4- Energy shift extraction
        # 5- Compute total power of energy shift and the %
        # 6- Increase % at initial window
        # 7- Decrease % at final window
        load,_ = self.get_selected_load(actions["load_selection"])
        # load,_ = self.get_selected_load()
        initial_window = self.get_initial_window(actions["initial_window"])
        final_window = self.get_final_window(actions["final_window"])
        energy_shift = self.get_energy_shift(actions["shift"])
        window_energy_shift_D = self.compute_shift_power(load,initial_window, energy_shift)
        # window_energy_shift_I = self.compute_shift_power(load,initial_window, energy_shift)
        self.LOADS_D[load].loc[initial_window:initial_window+3] += window_energy_shift_D*0.25
        if initial_window+final_window*4 > 0.94:
            # self.LOADS_D[load].loc[final_window:final_window+3] -= window_energy_shift*0.01
            self.LOADS_D[load].loc[4*(final_window-1):4*final_window-1] -= window_energy_shift_D*0.25
        else:
            self.LOADS_D[load].loc[initial_window+4*final_window:initial_window+3+4*final_window] -= window_energy_shift_D*0.25

        print(f"Sum of {load} domestic load: {self.LOADS_D[load].sum()}")
    
    def energy_shift_industrial(self, actions):
        # 1- load selection
        # 2- Initial window extraction
        # 3- Final window extraction
        # 4- Energy shift extraction
        # 5- Compute total power of energy shift and the %
        # 6- Increase % at initial window
        # 7- Decrease % at final window

        _,load = self.get_selected_load(actions["load_selection_2"])
        # _,load = self.get_selected_load()
        initial_window = self.get_initial_window(actions["initial_window_2"])
        final_window = self.get_final_window(actions["final_window_2"])
        energy_shift = self.get_energy_shift(actions["shift_2"])
        window_energy_shift_I = self.compute_shift_power(load,initial_window, energy_shift)
        self.LOADS_I[load].loc[initial_window:initial_window+3] += window_energy_shift_I*0.25
        if initial_window+final_window*4 > 0.94:
            # self.LOADS_D[load].loc[final_window:final_window+3] -= window_energy_shift*0.01
            self.LOADS_I[load].loc[4*(final_window-1):4*final_window-1] -= window_energy_shift_I*0.25
        else:
            self.LOADS_I[load].loc[initial_window+4*final_window:initial_window+3+4*final_window] -= window_energy_shift_I*0.25

        print(f"Sum of {load} industrial load: {self.LOADS_I[load].sum()}")

    def compute_new_domestic_states(self):
        aux_loads_d = self.LOADS_D.copy()
        aux_loads_d["mean_distribution"] = self.compute_domestic_mean_distribution()
        aux_loads_d["time"] = pd.date_range(start='2017-01-01', end='2017-01-02', periods=96)
        aux_loads_d = aux_loads_d.resample('H', on='time').mean()

        aux_loads_d.drop(aux_loads_d.tail(1).index,inplace=True)
        aux_loads_d.reset_index(inplace=True, drop=True)
        # print(sum(aux_loads_d[self.load].values))
        # return aux_loads_d[self.load].values / sum(aux_loads_d[self.load].values)
        return aux_loads_d["mean_distribution"].values
    
    def compute_new_industrial_states(self):
        aux_loads_i = self.LOADS_I.copy()
        aux_loads_i["mean_distribution"] = self.compute_industrial_mean_distribution()
        aux_loads_i["time"] = pd.date_range(start='2017-01-01', end='2017-01-02', periods=96)
        aux_loads_i = aux_loads_i.resample('H', on='time').mean()

        aux_loads_i.drop(aux_loads_i.tail(1).index,inplace=True)
        aux_loads_i.reset_index(inplace=True, drop=True)
        # print(sum(aux_loads_d[self.load].values))
        # return aux_loads_d[self.load].values / sum(aux_loads_d[self.load].values)
        return aux_loads_i["mean_distribution"].values
    
    def compute_new_states(self):
        states = {"domestic_loads":self.compute_new_domestic_states(),
                  "industrial_loads":self.compute_new_industrial_states()}
        return states

    def run_simulation(self, action=None):
        print("Run simulation")
        self.n_steps+=1
        self.energy_losses=[]
        self.energy_produced=[]
        self.energy_exported=[]
        self.energy_consumed=[]
        self.energy_exchanged=[]

        if action is not None:
            # self.LOADS_D = self.LOADS_D*self.get_actions()[action]
            # self.LOADS_I = self.LOADS_I*self.get_actions()[action]
            # print(self.LOADS_D.shape)
            # print(self.LOADS_I.head())
            self.energy_shift_performance(actions=action)
            self.energy_shift_industrial(actions=action)

        # ANALYSIS OVER 24 HOURS
        for i in range(np.shape(self.LOADS_D)[0]):  #for all the 24 hours
            # ASSIGN THE CORRECT POWER VALUE FOR EACH DOMESTIC LOAD
            loads_list = self.LOADS_D.columns
            consumed_timestemp = 0  # power consumed in this timestemp
            for j in range(len(loads_list)):
                self.net.load.p_mw.at[pp.get_element_index(self.net, "load", loads_list[j])] = float(self.LOADS_D.loc[i][str(loads_list[j])])/1000   #we have to divide for 1000 because in the file RESIDENTIAL.csv  it's expressed in kw
                self.net.load.q_mvar.at[pp.get_element_index(self.net, "load", loads_list[j])] = float(self.LOADS_D.loc[i][str(loads_list[j])])*np.tan(np.arccos(0.9))/1000
                consumed_timestemp += float(self.LOADS_D.loc[i][str(loads_list[j])])/1000

            # ASSIGN THE CORRECT POWER VALUE FOR EACH INDUSTRIAL LOAD
            loads_list = self.LOADS_I.columns
            for z in range(len(loads_list)):
                self.net.load.p_mw.at[pp.get_element_index(self.net, "load", loads_list[z])] = float(self.LOADS_I.loc[i][str(loads_list[z])])/1000
                self.net.load.q_mvar.at[pp.get_element_index(self.net, "load", loads_list[z])] = float(self.LOADS_I.loc[i][str(loads_list[z])])/1000  * np.tan(np.arccos(0.9))
                consumed_timestemp += float(self.LOADS_I.loc[i][str(loads_list[z])])/1000

            # ASSIGN THE CORRECT POWER VALUE FOR EACH PV
            pv_list = self.PV.columns
            produced_timestemp=0   # power produced in this timestemp
            
            for k in range(len(pv_list)):
                float(self.PV.loc[i][str(pv_list[k])]) / 1000
                self.net.sgen.p_mw.at[pp.get_element_index(self.net, "sgen", pv_list[k])] = float(self.PV.loc[i][str(pv_list[k])]) / 1000
                produced_timestemp += float(self.PV.loc[i][str(pv_list[k])]) / 1000

            # SOLVE THE SNAP SHOT OF THE CIRCUIT
            pp.runpp(self.net)  # solve the circuit

            #  UPDATE KPIs
            self.energy_produced.append(float(produced_timestemp)/4)
            self.energy_consumed.append(float(consumed_timestemp)/4)

            power_exchange = self.net.res_ext_grid.p_mw.at[0]  # this is the connection line with the external grid
            losses_timestemp = abs(power_exchange + produced_timestemp - consumed_timestemp)
            self.energy_losses.append(losses_timestemp/4)
            self.energy_exchanged.append(float(power_exchange/4))
            if power_exchange>0:
                power_exchange=0
            else:
                power_exchange=-power_exchange
            self.energy_exported.append(power_exchange/4)

        if sum(self.energy_produced)>0:
            self.SCR=100*(1-(sum(self.energy_exported)/sum(self.energy_produced)))
        else:
            self.SCR=0
        self.SSR=100*((sum(self.energy_produced)-sum(self.energy_exported))/(sum(self.energy_consumed)+sum(self.energy_losses)))

        self.__simulation_logs__()
        finish = self.finish_simulation()

        # return np.array([sum(self.energy_produced), sum(self.energy_consumed)]), finish
        # return np.array([sum(self.energy_exchanged)]), finish
        return self.compute_new_states(), finish
