import json
from tensorforce import Agent

class TensorforceAgent():
    def __init__(self, agent_path, environment, network='auto') -> None:
        self.agent_path = agent_path
        self.load_agent(environment, network)
    
    def load_agent(self, environment, network):
        if "ppo" in self.agent_path:
            self.agent = Agent.create(
                            agent=self.agent_path,
                            environment=environment,
                            network=network,
                            max_episode_timesteps=environment.max_episode_timesteps()
                        )
        else:
            self.agent = Agent.create(
                            agent=self.agent_path,
                            environment=environment,
                            policy=network,
                            max_episode_timesteps=environment.max_episode_timesteps()
                        )

    def get_agent(self):
        return self.agent


# agent_path = "/home/jorge_mira/projects/IoT-NGIN/RL-smart-energy/src/agents/agent_ppo.json"
# environment = SmartGridEnvironment(max_steps=25)
# tf_agent = TensorforceAgent(agent_path, environment)
# print("success")