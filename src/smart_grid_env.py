from tensorforce.environments import Environment
from src.simulator.rl_smart_grid_simulator import SmartGridSimulator

class SmartGridEnvironment(Environment):

    def __init__(self,hours=4,static_flag=True, max_steps=20):
        super().__init__()
        self.SSR = None
        self.SCR = None
        self.hours = hours
        self.static_flag=static_flag
        self.max_steps = max_steps
        self.restart_environment()

    def restart_environment(self):
        self.simulator = SmartGridSimulator(max_steps=self.max_steps)
        states = self.simulator.compute_new_states()
        return states
        # return {
        #     "domestic_loads":states[0],
        #     "industrial_loads":states[1]
        # }

    def states(self):
        """
        States: (
            Domestic load distribution,
            Industrial load distribution
            )
        """
        # return dict(type='float', shape=(2,))
        # return dict(type='float', shape=(1,))
        # return dict(type='float', shape=(24,))
        return {
            "domestic_loads":dict(type='float', shape=(24,)),
            "industrial_loads":dict(type='float', shape=(24,))
        }

    def actions(self):
        """
        Actions:
        """
        # return dict(type='int', num_values=2)
        return {
            "load_selection":dict(type="int", num_values=2),
            "load_selection_2":dict(type="int", num_values=2),
            "initial_window":dict(type="int", num_values=24),#"initial_window":dict(type="int", num_values=24),
            "initial_window_2":dict(type="int", num_values=24),#"initial_window":dict(type="int", num_values=24),
            "final_window":dict(type="int", num_values=2),
            "final_window_2":dict(type="int", num_values=2),
            "shift":dict(type="int", num_values=2),
            "shift_2":dict(type="int", num_values=2),
        }

    # Optional: should only be defined if environment has a natural fixed
    # maximum episode length; restrict training timesteps via
    #     SmartGridEnvironment.create(..., max_episode_timesteps=???)
    def max_episode_timesteps(self):
        return self.simulator.max_steps

    # Optional additional steps to close environment
    def close(self):
        super().close()

    def reset(self):
        state = self.restart_environment()
        return state

    def execute(self, actions):
        next_state, terminal = self.simulator.run_simulation(actions)
        reward = self.reward()
        return next_state, terminal, reward

    def reward(self):
        return 0.5*(self.simulator.SCR + self.simulator.SSR)/100
