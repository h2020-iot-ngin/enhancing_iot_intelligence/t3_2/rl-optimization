import tensorflow as tf

def custom_network_builder(state, num_actions, network_config, **kwargs):
    input_layer = tf.layers.flatten(state["my_observation"])

    hidden_layer = tf.layers.dense(input_layer, units=64, activation=tf.nn.relu)

    output_layer = tf.layers.dense(hidden_layer, units=num_actions, activation=None)

    return output_layer, hidden_layer