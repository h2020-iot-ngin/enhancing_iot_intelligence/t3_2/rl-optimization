import logging
import tensorflow as tf

class Callbacks():

    def __init__(self, log_dir) -> None:
        """
        Args:
            tb_writer: tensorboard logger instance
        """
        super().__init__()
        self.log_dir = log_dir
        self.episode = None
        self.step = None
        self.graph_name = None

    def on_episode_start(self, num_episode, states):
        logging.info(f"Starting episode {num_episode+1}")
        self.episode = num_episode
        self.step = 0
        self.graph_name = "Episode_" #+str(self.episode)+"_"
        self.tb_writer = tf.summary.create_file_writer(self.log_dir+"/episode-"+str(self.episode))
        for state in states:
            for hour, load in enumerate(states[state]):
                with self.tb_writer.as_default():
                    tf.summary.scalar(self.graph_name+"initial_"+str(state)+"_distribution",
                                    load,
                                    hour)

    def on_step_end(self, reward):
        self.step += 1
        with self.tb_writer.as_default():
            tf.summary.scalar(self.graph_name+"reward", reward, self.step)

    def on_episode_end(self, reward, states):
        with self.tb_writer.as_default():
            tf.summary.scalar(self.graph_name+"final_reward", reward, self.episode)

        for state in states:
            for hour, load in enumerate(states[state]):
                with self.tb_writer.as_default():
                    tf.summary.scalar(self.graph_name+"final_"+str(state)+"_distribution",
                                    load,
                                    hour)
    
    def on_best_reward(self, states):
        for state in states:
            for hour, load in enumerate(states[state]):
                with self.tb_writer.as_default():
                    tf.summary.scalar(self.graph_name+"best_"+str(state)+"_distribution",
                                    load,
                                    hour)